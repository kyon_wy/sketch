import base64
import caffe
from cv2 import imread, cvtColor, COLOR_BGR2GRAY
import datetime
from flask import Flask, render_template, request, make_response, jsonify
import numpy as np
import optparse
import os
#from skimage.io import imread
from string import split
import tornado.wsgi
import tornado.httpserver
import urllib
import werkzeug
#import exifutil

from sbir import SBIRWarpper

UPLOAD_FOLDER = '/tmp/caffe_demos_uploads'

app = Flask(__name__)

def start_tornado(app, port=5000):
    http_server = tornado.httpserver.HTTPServer(
        tornado.wsgi.WSGIContainer(app))
    http_server.listen(port)
    print("Tornado server starting on port {}".format(port))
    tornado.ioloop.IOLoop.instance().start()


def start_from_terminal(app):
    """
    Parse command line options and start the server.
    """
    parser = optparse.OptionParser()
    parser.add_option(
        '-d', '--debug',
        help="enable debug mode",
        action="store_true", default=False)
    parser.add_option(
        '-p', '--port',
        help="which port to serve content on",
        type='int', default=5000)
    parser.add_option(
        '-g', '--gpu',
        help="use gpu mode",
        action='store_true', default=False)

    opts, args = parser.parse_args()

    deploy_file = './sbir/data/models/sketch_fc7_deploy.prototxt'
    # Initialize shoes classifer
    feat_path = './sbir/data/cache/shoes_image_feats.mat'
    model_path = './sbir/data/models/shoes.caffemodel'
    list_path = './sbir/data/lists/shoes_list.txt'
    app.shoes = SBIRWarpper(model_path, list_path, feat_path, deploy_file, opts.gpu)

    # Initialize chairs classifer
    feat_path = './sbir/data/cache/chairs_image_feats.mat'
    model_path = './sbir/data/models/chairs.caffemodel'
    list_path = './sbir/data/lists/chairs_list.txt'
    app.chairs = SBIRWarpper(model_path, list_path, feat_path, deploy_file, opts.gpu)

    if opts.debug:
        app.run(debug=True, host='0.0.0.0', port=opts.port)
    else:
        start_tornado(app, opts.port)


@app.route("/")
def index():
    print 'enter index()'
    return render_template("index.html")


@app.route("/", methods = ['POST'])
def getSketch():
    print 'enter getSketch()'
    data = request.form['image']
    sketchType = request.form['type']

    suffix = split(split(data, ';')[0], '/')[1]
    b64_string = split(data, ',')[1]
    sketch = base64.decodestring(b64_string)

    result = []

    filename_ = str(datetime.datetime.now()).replace(' ', '_') + '.' + suffix
    filename = os.path.join(UPLOAD_FOLDER, filename_)

    with open(filename, 'wb') as f:
        f.write(sketch)

    rgb = imread(filename)
    image = cvtColor(rgb, COLOR_BGR2GRAY)
    #image = caffe.io.load_image(filename)
    if sketchType == 'shoe':
        origin = app.shoes.run_retrieval(image)
        result = map(addShoePrefix, origin)
    elif sketchType == 'chair':
        origin = app.chairs.run_retrieval(image)
        result = map(addChairPrefix, origin)

    #result = app.clf.classify_image(image)

    resp = jsonify(images=result)

    return resp

def addShoePrefix(val):
    return '/static/sbir/shoes/' + val

def addChairPrefix(val):
    return '/static/sbir/chairs/' + val

if __name__ == "__main__":
    if not os.path.exists(UPLOAD_FOLDER):
        os.makedirs(UPLOAD_FOLDER)
    start_from_terminal(app)
    #app.run(host='0.0.0.0')
