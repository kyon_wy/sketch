import 'normalize.css'
import '../bower_components/bootstrap-css/css/bootstrap.min.css'
import './style/index.styl'
import '../bower_components/lightbox2/src/js/lightbox.js'
import '../bower_components/lightbox2/src/css/lightbox.css'

import $ from 'jquery'
import React from 'react'
import ReactDOM from 'react-dom'
import Helmet from 'react-helmet'
import InputForm from './InputForm'
import Result from './Result'

class App extends React.Component {

  constructor() {
    super()

    this.init()
  }

	init() {
    this.state = {
      dropDownTitle: "select your paint type(default is chair)"
    , sketchType: "chair" //default type
    , upload: false
    , filename: undefined

    , showResults: false
    , totalResults: []
    , perPage: 15
    , painting: undefined
    }
	}

  handleReset(e) {
    e.preventDefault()

    this.setState({ showResults: false, upload: false })
  }

  handleBack(e) {
    e.preventDefault()

    var img = new Image()
    img.src = this.state.painting
  }

  handleResults(origin, results) {
    this.setState({ totalResults: results
                  , painting: origin
                  , showResults: true
                  })
  }

  handleUpload(isUploaded) {
    this.setState({ upload: isUploaded })
  }

  changeSelected(e) {
    e.preventDefault()
    var sketchType = e.target.text
      , title = sketchType
      , perPage = 15

    if (sketchType === 'shoe')
      perPage = 15
    else
      perPage = 6

    this.setState({ sketchType: sketchType
                  , dropDownTitle: title
                  , perPage: perPage })
  }

  setFilename(filename) {
    this.setState({ filename: filename
                  , upload: true
                  })
  }

  render() {
    var content
    if (!this.state.showResults) {
      content = <InputForm receiveResults={ ::this.handleResults }
                  paint={ this.state.painting }
                  dropDownTitle={ this.state.dropDownTitle }
                  sketchType={ this.state.sketchType }
                  changeSelected={ ::this.changeSelected }
                  handleReset={ ::this.handleReset }
                  setFilename={ ::this.setFilename }/>
    } else {
      content =
          <Result
            painting={ this.state.painting }
            handleReset={ ::this.handleReset }
            totalResults={ this.state.totalResults }
            perPage={ this.state.perPage}
            upload={ this.state.upload }
            filename={ this.state.filename }/>
    }

    return (
      <div className="container">
        <Helmet
          title="sketch"
          meta={ [{ "name": "viewport"
                  , "content": "width=device-width, initial-scale=1.0"
                  }] }/>
        { content }
      </div>
    )
  }
}


$('document').on('click', 'a[href="#"]', (e) => {
   e.preventDefault()
})

var appViewWrapper = $('<div>', { 'class': 'react-container' })

$('body').prepend(appViewWrapper)

ReactDOM.render(<App />, appViewWrapper[0])
