import React from 'react'
import ReactDOM from 'react-dom'
import { DropdownButton, MenuItem } from 'react-bootstrap'

export default class extends React.Component {

  constructor() {
    super()

    this.context = undefined
    this.theCanvas = undefined
    this.isPainting = false
    this.uploader = undefined

    this.state = {
      width: 256
    , height: 256
    }
  }

  componentDidMount() {
	  var theCanvas = ReactDOM.findDOMNode(this.refs.canvas)
	  if (theCanvas) {
	    this.context = theCanvas.getContext('2d')
	    this.theCanvas = theCanvas
	    this.drawScreen(this.state.width, this.state.height)
	  }
    this.uploader = ReactDOM.findDOMNode(this.refs.uploader)

    if (this.props.paint) {
      var img = new Image()

      img.src = this.props.paint

      this.context.drawImage(img, 0, 0)
    }
  }

  drawScreen(width, height) {
    this.context.fillStyle = 'white';
    this.context.fillRect(0, 0, width, height);
  }

  reset(e) {
    this.props.handleReset(e)
    this.uploader.value = ''
    this.setState({ width: 256, height: 256 })
    this.drawScreen(256, 256)
  }

  handleMouseDown(e) {
    var context = this.context
      , originX = this.theCanvas.offsetLeft
      , originY = this.theCanvas.offsetTop
      , x = e.pageX - originX
      , y = e.pageY - originY

    this.isPainting = true
    this.startPaint(context, x, y)
  }

  handleMouseLeave(e) {
    this.isPainting = false
  }

  startPaint(context, x, y) {
    context.beginPath()
    context.moveTo(x, y)
  }

  paint(context, x, y) {
    context.lineTo(x, y)
    context.stroke()
  }

  handleMouseMove(e) {
    if (this.isPainting) {
      e.preventDefault()

      var context = this.context
        , originX = this.theCanvas.offsetLeft
        , originY = this.theCanvas.offsetTop
        , x = e.pageX - originX
        , y = e.pageY - originY

      this.paint(context, x, y)
    }
  }

  handleTouchMove(e) {
    if (this.isPainting) {
      e.preventDefault()

      var context = this.context
        , originX = this.theCanvas.offsetLeft
        , originY = this.theCanvas.offsetTop
        , x = e.targetTouches.item(0).pageX - originX
        , y = e.targetTouches.item(0).pageY - originY

      this.paint(context, x, y)
    }
  }

  handleMouseUp(e) {
    this.isPainting = false
  }

  handleSubmit(e) {
    e.preventDefault()

    var url = "/"
      , xhr = new XMLHttpRequest()
      , data = undefined
      , formData = new FormData()
      , painting = this.theCanvas.toDataURL('image/jpeg')

    formData.append('image', painting)
    formData.append('type', this.props.sketchType)

    xhr.open('POST', url, true)
    xhr.responseType = 'json'

    xhr.addEventListener('load', () => {
      if(xhr.status >= 200 && xhr.status < 300) {
        this.props.receiveResults(painting, xhr.response.images)
      } else {
        console.log(xhr.status)
      }
    })

    xhr.send(formData)
  }

  resizeContainer(img, maxWidth) {
    var showWidth = 0
      , showHeight = 0

    showWidth = Math.min(maxWidth, img.width)

    if (showWidth == maxWidth)
      showHeight= img.height * maxWidth / img.width
    else
      showHeight = img.height

    return {showWidth, showHeight}
  }

  handleFile(e) {
    var file = e.target.files[0]
      , img = new Image()
      , reader = new FileReader()
      , context = this.context
      , w = this.state.width
      , h = this.state.height
      , dim
      , tmp
      , filename

    tmp = file.name.split('/')
    filename  = tmp[tmp.length - 1]
    this.props.setFilename(filename)

    //img.file = file
    reader.onload = ((aImg) => {
      return (e) => {
          aImg.src = e.target.result
      }
    })(img)

    reader.readAsDataURL(file)

    img.addEventListener('load', () => {
      dim = this.resizeContainer(img, w)
      w = dim.showWidth
      h = dim.showHeight
      this.setState({ width: w, height: h })

      this.drawScreen(w, h)
      context.drawImage(img, 0, 0, w, h)
    })

  }

  render() {
    return (
      <div>
        <form className="inputForm"
              onSubmit={ ::this.handleSubmit }
              onReset={ ::this.reset }>
          <div className="form-group">
            <div className="canvas-container"
               width={ this.state.width + 'px' }
               height={ this.state.height + 'px' }>
              <canvas className='the-canvas' ref="canvas"
                      width={ this.state.width + 'px' }
                      height={ this.state.height + 'px' }
                      onMouseMove={ ::this.handleMouseMove }
                      onMouseDown={ ::this.handleMouseDown }
                      onMouseLeave={ ::this.handleMouseLeave }
                      onMouseOut={ ::this.handleMouseLeave }
                      onMouseUp={ ::this.handleMouseUp }
                      onTouchStart={ ::this.handleMouseDown}
                      onTouchMove={ ::this.handleTouchMove }
                      onTouchEnd={ ::this.handleMouseUp }
                      onTouchCancel={ ::this.handleMouseLeave }>
                Your browser does not support the HTML5 Canvas.
              </canvas>
            </div>
          </div>
          <div className="form-group">
            <DropdownButton title={ this.props.dropDownTitle }
               id="0" bsStyle='default'
               onSelect={ ::this.props.changeSelected }
               ref="dropdown">
              <MenuItem eventKey="1">shoe</MenuItem>
              <MenuItem eventKey="2">chair</MenuItem>
            </DropdownButton>
          </div>
          <div className="form-group">
            <input className="btn btn-default btn-file"
                   onChange={ ::this.handleFile }
                   accept="image/*"
                   ref="uploader"
                   type="file" />
            <button className="btn btn-default reset"
                    type="reset">reset</button>
            <button className="btn btn-default submit"
                    type="submit">submit</button>
          </div>
        </form>
      </div>
    )
  }
}
