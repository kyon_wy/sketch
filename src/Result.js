import React from 'react'
import ReactDOM from 'react-dom'

export default class extends React.Component {

  render() {
    return (
      <div className="row">
        <div className="col-xs-3">
          <Origin width={ this.props.width } height={ this.props.height }
            painting={ this.props.painting }
            handleReset={ this.props.handleReset }/>
        </div>
        <div className="col-xs-9">
          <ImgList results={ this.props.totalResults }
            upload={ this.props.upload } filename={ this.props.filename }/>
        </div>
      </div>
    )
  }
}

class Origin extends React.Component {
  render() {
    return (
      <div className="origin-container">
        <div className="canvas-container">
          <img ref='origin' className="origin"
            src={ this.props.painting } />
        </div>
        <button className="back btn btn-default"
              onClick={ this.props.handleReset }>back</button>
      </div>
    )
  }
}

class ImgList extends React.Component {

  renderResult(val, index) {
    var legend = index + 1
      , klass = "result-container "

    if (this.props.upload) {
      var tmp = val.split('/')
        , cur = tmp[tmp.length - 1]
      if (cur === this.props.filename) {
        console.log('active', cur)
        klass += "active"
      }
    }

    return (
      <li className="item" key={ index }>
        <div className={ klass }>
        <a href={ val } data-lightbox="result">
          <img className="result-img" src={ val } />
        </a>
        <span className="result-legend">{ legend }</span>
        </div>
      </li>
    )
  }

  render() {
    return (
      <div>
        <ul className="images">
          { this.props.results.map(::this.renderResult) }
        </ul>
      </div>
    )
  }
}
