import numpy as np
from scipy.io import loadmat
from time import time
from util import my_load_mat


class SampleProvider:
    def __init__(self, meta, num_cls):
        self.bm_list = []
        self.create_block_mangers(meta)
        if len(self.bm_list) != num_cls:
            raise Exception("bm_list != num_cls")

    def create_block_mangers(self, meta):
        for m in meta:
            self.bm_list.append(MemoryBlockManger(m['data_path'], m['mean_path'], False))

    def get_category_instance(self, cls_id):
        """return a sample of specified category."""
        cls_id = int(cls_id)
        return self.bm_list[cls_id].pop_sample()



class MemoryBlockManger:
    def __init__(self, data_path, mean, has_label=True, batchsize=None, mean_value=True):
        self.verbose = False
        self.mean = None
        self.data_path = data_path
        self.num_samples = 0
        self.sample_id = 0
        self.batch_data = None
        self.batch_label = None
        self.db_has_label = None
        self.use_mean_value = True
        self.batch_size = batchsize
        self.has_label = has_label
        self.load_data_block()
        if not self.use_mean_value:
            self.set_mean(mean)

    def pop_sample(self):
        if self.sample_id >= self.num_samples:
            self.sample_id = 0
        self.sample_id = self.sample_id + 1
        if self.has_label and self.db_has_label:
            return self.batch_data[self.sample_id-1, ::], \
                   self.batch_label[self.sample_id-1]
        elif self.has_label and not self.db_has_label:
            return self.batch_data[self.sample_id-1, ::], []
        else:
            return self.batch_data[self.sample_id-1, ::]

    def get_sample_by_ind(self, inds):
        if self.has_label and self.db_has_label:
            return self.batch_data[inds, ::], \
                   self.batch_label[inds]
        elif self.has_label and not self.db_has_label:
            return self.batch_data[inds, ::], []
        else:
            return self.batch_data[inds, ::]

    def pop_batch(self):
        if self.sample_id >= self.num_samples:
            return None
        this_batch_size = min(self.batch_size, self.num_samples-self.sample_id)
        prev_sample_id = self.sample_id
        self.sample_id += this_batch_size
        data = self.batch_data[prev_sample_id: self.sample_id, ::]
        if self.has_label and self.db_has_label:
            labels = self.batch_label[prev_sample_id: self.sample_id]
            return data, labels
        elif self.has_label and not self.db_has_label:
            return data, []
        else:
            return data

    def eof(self):
        return self.sample_id >= self.num_samples

    def load_data_block(self):
        print 'loading data %s' % self.data_path
        t = time()
        dic = my_load_mat(self.data_path)
        self.num_samples = dic['data'].shape[0]
        self.batch_data = dic['data']
        self.db_has_label = True
        try:
            if self.has_label:
                labels = dic['labels']
                if labels.min() == 1:
                    labels = labels - 1
                self.batch_label = labels
        except:
            self.db_has_label = False
        print 'data loaded %s (%0.2f sec.)' % (
            self.get_data_shape_str(self.batch_data), time()-t)

    def get_data_shape_str(self, data):
        s = ''
        shape = data.shape
        for d in shape:
            s += '%dx' % d
        s = s[:-1]
        return s

    def set_mean(self, mean):
        raise Exception("disabled")


def block_manger_debugger():
    data_dir = 'D:/projects/workspace/sketch_data/tmp'
    num_batches = 10
    name_format = 'airplane_block%02d.mat'
    bm = MemoryBlockManger(data_dir)
    while True:
        bm.pop_sample()


if __name__ == "__main__":
    block_manger_debugger()