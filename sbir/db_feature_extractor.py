from test_model import extract_sketch_features
from scipy.io import savemat
import caffe


caffe.set_mode_gpu()
caffe.set_device(0)


def extract_multiview_feature(weight_path, sketch_db_path, sv_path):
    sketch_info = {'proto': './data/models/sketch_fc7_norm_deploy.prototxt', 'weights': weight_path}
    db_info = {'mean_val': 250.42, 'data_path': sketch_db_path}
    sketch_feats, sketch_labels = extract_sketch_features(sketch_info, db_info, multiview=True)
    savemat(sv_path, {'im_feats': sketch_feats})


if __name__ == "__main__":
    # dump shoes features
    sketch_db_path = '/homes/fl302/data/sbir/edges/shoes_edge_db_all.mat'
    weight_path = './data/models/shoes.caffemodel'
    sv_path = './data/cache/shoes_image_feats.mat'
    extract_multiview_feature(weight_path, sketch_db_path, sv_path)

    # dump chair features
    sketch_db_path = '/homes/fl302/data/sbir/edges/chairs_edge_db_all.mat'
    weight_path = './data/models/chairs.caffemodel'
    sv_path = './data/cache/chairs_image_feats.mat'
    extract_multiview_feature(weight_path, sketch_db_path, sv_path)