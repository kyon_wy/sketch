import caffe
import datetime
import numpy as np
from scipy.io import loadmat
from skimage.transform import resize
from sklearn.preprocessing import normalize
from test_model import do_multiview_crop, compute_view_specific_distance
from util import load_list

class SBIRWarpper:
    def __init__(self, weight_path, list_path, feat_path, deploy_file, gpu_mode):
        self.image_names = load_list(list_path)
        self.image_feats = loadmat(feat_path)['im_feats']
        self.net_ = None
        self.input_dim = 256
        self.crop_dim = 225
        self.mean_val = 250.42
        self.feat_layer = 'fc7_sketch'
        self.net_ = caffe.Net(deploy_file, weight_path, caffe.TEST)
        
        if gpu_mode:
            caffe.set_mode_gpu()
        else:
            caffe.set_mode_cpu()

    def run_retrieval(self, im):
        if im.shape[0] != self.input_dim:
            print('resized')
            im = resize(im, (self.input_dim, self.input_dim))
        # feature extraction
        im = im.astype(np.single) - self.mean_val
        transformed = do_multiview_crop(im, self.crop_dim)
        num, chns, rows, cols = transformed.shape
        self.net_.blobs['data'].reshape(*(num, chns, rows, cols))
        output = self.net_.forward(data=transformed.astype(np.float32, copy=False))
        target = output[self.feat_layer].copy()
        target = normalize(target.reshape((num, -1)))
        # query
        multiview_dists = compute_view_specific_distance(target, self.image_feats)
        ave_dist = multiview_dists.mean(axis=0)
        ranking_order = ave_dist.flatten().argsort()
        # get image names
        return [self.image_names[i] for i in ranking_order]

