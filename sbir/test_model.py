import os
import numpy as np
from time import time
import scipy.spatial.distance as ssd
import caffe
from batch_manger import MemoryBlockManger

# set caffe
# if gpu:
#     caffe.set_mode_gpu()
#     caffe.set_device(2)

def set_model(model_info):
    net_ = caffe.Net(model_info['proto'], model_info['weights'], caffe.TEST)
    return net_


def load_mean_value(mean):
    if type(mean) == str:
        m = np.load(mean)
        return m.mean(-1).mean(-1).reshape(1, 3, 1, 1)
    else:
        return mean


def do_center_crop(data, cropsize):
    if len(data.shape) == 3: # sketch
        n, h, w = data.shape
        data = data.reshape((n, 1, h, w))
    n, c, h, w = data.shape
    batch_data = np.zeros((n, c, cropsize, cropsize), np.single)
    x = int((h - cropsize) * 0.5)
    for i in xrange(n):
        batch_data[i, :, :, :] = data[i, :, x:x+cropsize, x:x+cropsize]
    return batch_data


def do_multiview_crop(data, cropsize):
    if len(data.shape) == 2: # single sketch
        data = data[np.newaxis, np.newaxis, :, :]  # nxcxhxw
    elif len(data.shape) == 3: # sketch
        n, h, w = data.shape
        data = data.reshape((n, 1, h, w))
    n, c, h, w = data.shape
    xs = [0, 0, w-cropsize, w-cropsize]
    ys = [0, h-cropsize, 0, h-cropsize]
    batch_data = np.zeros((n*10, c, cropsize, cropsize), np.single)
    y_cen = int((h - cropsize) * 0.5)
    x_cen = int((w - cropsize) * 0.5)
    for i in xrange(n):
        for (k, (x, y)) in enumerate(zip(xs, ys)):
            batch_data[i*10+k, :, :, :] = data[i, :, y:y+cropsize, x:x+cropsize]
        # center crop
        batch_data[i*10+4, :, :, :] = data[i, :, y_cen:y_cen+cropsize, x_cen:x_cen+cropsize]
        for k in xrange(5):  # flip
            batch_data[i*10+k+5, :, :, :] = batch_data[i*10+k, :, :, ::-1]
    return batch_data


def get_feature_dims(net_, layer_name):
    return net_.blobs[layer_name].channels*net_.blobs[layer_name].height*net_.blobs[layer_name].width


def extract_features(net_, data_info, cropsize, layer_name, multi_view=False):
    # load dataset
    data_path = data_info['data_path']
    mean = data_info['mean_val'] if 'mean_val' in data_info else data_info['mean_path']
    db = MemoryBlockManger(data_path, mean, True, 128)
    mean_value = load_mean_value(mean)

    # featrue extraction
    t = time(); labels = []
    num_samples = 10*db.num_samples if multi_view else db.num_samples
    feats = np.zeros((num_samples, get_feature_dims(net_, layer_name)), np.single)
    idx = 0
    while not db.eof():
        batch_data, batch_labels = db.pop_batch()
        if multi_view:
            transformed = do_multiview_crop(batch_data, cropsize)
        else:
            transformed = do_center_crop(batch_data, cropsize)
        transformed = transformed - mean_value
        num, chns, rows, cols = transformed.shape
        net_.blobs['data'].reshape(*(num, chns, rows, cols))
        output = net_.forward(data=transformed.astype(np.float32, copy=False))
        target = output[layer_name]
        if len(target.shape) == 4:
            n, c, h, w = target.shape
            feats[idx:idx+num, ::] = target.reshape(n, c * h * w)
        else:
            feats[idx:idx+num, ::] = target
        # feats.append(output[layer_name])
        labels.append(batch_labels)
        idx += num
    print 'feature computation completed (%0.2f sec.)' % (time()-t)
    return feats, np.concatenate(labels, 0)


def extract_sketch_features(model_info, dbinfo, layer_name='norm7_sketch', multiview=False):
    sketch_net = set_model(model_info)
    return extract_features(sketch_net, dbinfo, 225, layer_name, multi_view=multiview)


def reshape_multiview_features(feats):
    n, c = feats.shape
    feats = feats.reshape(n/10, 10, c)
    return feats


def compute_view_specific_distance(sketch_feats, image_feats):
    sketch_feats = reshape_multiview_features(sketch_feats)
    image_feats = reshape_multiview_features(image_feats)
    num_sketches, num_images = sketch_feats.shape[0], image_feats.shape[0]
    num_views = 10
    multi_view_dists = np.zeros((num_views*2, num_sketches, num_images))
    for i in xrange(num_views):
        multi_view_dists[i, ::] = ssd.cdist(sketch_feats[:, i, :], image_feats[:, i, :])
        multi_view_dists[i+num_views, ::] = ssd.cdist(sketch_feats[:, i, :], image_feats[:, -i, :])
    return multi_view_dists