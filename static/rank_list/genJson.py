#! /usr/bin/env/python

result = {}

for i in range(1, 50):
    key = `i` +'_sketch.jpg'
    result[key] = [ `i` + '_rank' + `k` for k in range(1, 11)]

print result
