def addPrefix(jpg):
    return '/static/rank_list/' + jpg

with open("image_rank_list.txt") as f:
    count = 1
    line = f.readline()
    sketch = line.split(":")[0]
    photo = ','.join([addPrefix(jpg) for jpg in line.split(":")[1][:-2].split(',')])
    while (line):
        if (line.find(":") != -1):
            sketch = line.split(":")[0]
            photo = ','.join([addPrefix(jpg) for jpg in line.split(":")[1][:-2].split(',')])
            count = 1
        filename = sketch + '_' + `count`
        with open(filename, 'w') as fw:
            fw.write(photo)
        count += 1
        line = f.readline()
        photo = ','.join([addPrefix(jpg) for jpg in line[:-2].split(',')])
